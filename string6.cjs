function camelCaseToSnakeCase(getString) {
    let snakeCase = '';

    for (const character of getString) {
        if (character === character.toLowerCase()) {
            snakeCase += character;
        } else {
            snakeCase += '_';
            snakeCase += character;
        }
    }

    return snakeCase;
}

module.exports = camelCaseToSnakeCase;
