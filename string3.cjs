function getMonth(testString){
    const dateSplitArray = testString.split('/');
    return Number(dateSplitArray[1]);
}

module.exports = getMonth;