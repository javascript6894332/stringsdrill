function getNumbers(testStringArray) {
    const updatedNumberArray = []; // Declare updatedNumberArray variable which will contain the result

    if (Array.isArray(testStringArray)) {
        for (let index = 0; index < testStringArray.length; index++) {  // loop 1- iterating through array
            let value = testStringArray[index];
            let element = ''; // Declare element variable
            if (typeof value === 'string') {
                for (let stringIndex = 0; stringIndex < value.length; stringIndex++) {  // loop 2 - iterating through element of array which is string
                    const charCode = value[stringIndex].charCodeAt();

                    if ((charCode >= 48 && charCode <= 57) || value[stringIndex] === '.') {  // checking fpr numerical values
                        element += value[stringIndex];
                    }
                }
            } else {
                return [0]
            }

            if (element.length > 0) {   // converting string into their integers/ decimal form
                updatedNumberArray.push(parseFloat(element));
            } else {
                updatedNumberArray.push(0);
            }
        }

        return updatedNumberArray;   // returning result
    } else {
        return [0];
    }
}

module.exports = getNumbers;
