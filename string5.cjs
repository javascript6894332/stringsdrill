function getSentence(testArray){
    const sentence = testArray.join(' ');
    return sentence;
}

module.exports = getSentence;
