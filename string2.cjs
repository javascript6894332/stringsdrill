function ipAddressSplit(testString) {
    const ipv4Array = testString.split('.');

    // Check if the IPv4 address has exactly 4 parts
    if (ipv4Array.length !== 4) {
        return [];
    } else {
        for (let index = 0; index < 4; index++) {
            // Check if the part is a valid number
            if (!Number(ipv4Array[index])) {
                return [];
            } else {
                // Check if the number is within the valid range (0-255)
                if (Number(ipv4Array[index]) >= 0 && Number(ipv4Array[index]) < 256) {
                    ipv4Array[index] = Number(ipv4Array[index]);
                } else {
                    return [];
                }
            }
        }
        return ipv4Array;
    }
}

module.exports = ipAddressSplit;
