function getFullName(testObject){
    fullName = '';
    for (key in testObject) {
        fullName += (testObject[key]).toUpperCase();
        fullName += ' ';
    }
    return fullName.substr(0,fullName.length-1);
}

module.exports = getFullName;
