// Define a string 
const testArray = ["the", "quick", "brown", "fox"];

// Import the string5 module, which contains the getSentence function
const string5 = require('../string5.cjs');

// Call the getSentence function from the imported module, passing the testArray array
const result = string5(testArray);

// Log the result
console.log(result);
