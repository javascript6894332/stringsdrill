// Define a string 
const testString = "111.139.161.143";

// Import the string2 module, which contains the ipAddressSplit function
const string2 = require('../string2.cjs');

// Call the ipAddressSplit function from the imported module, passing the testString array
const result = string2(testString);

// Log the result
console.log(result);
