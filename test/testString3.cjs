// Define a string 
const testString = "10/1/2021";

// Import the string3 module, which contains the getMonth function
const string3 = require('../string3.cjs');

// Call the getMonth function from the imported module, passing the testString array
const result = string3(testString);

//define an array of month name
const months = [
    "January", "February", "March", "April",
    "May", "June", "July", "August",
    "September", "October", "November", "December"
];
// Log the result
console.log(months[result-1]);
