// Define an array of strings 
const testString = ["$100.45", "$1,002.22", "-$123"];

// Import the string1 module, which contains the getNumbers function
const string1 = require('../string1.cjs');

// Call the getNumbers function from the imported module, passing the testString array
const result = string1(testString);

// Log the result
console.log(result);
