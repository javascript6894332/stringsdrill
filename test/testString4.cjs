// Define an object
const testObject =  {"first_name": "JoHN", 
                    "middle_name": "doe", "last_name": "SMith"};

// Import the string4 module, which contains the getFullName function
const string4 = require('../string4.cjs');

// Call the getFUllName function from the imported module, passing the testString array
const result = string4(testObject);

// Log the result
console.log(result);
