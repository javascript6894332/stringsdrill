// Define a string 
const testString = 'thisIsATest';

// Import the string5 module, which contains the getSentence function
const string6 = require('../string6.cjs');

// Call the getSentence function from the imported module, passing the testString 
const result = string6(testString);

// Log the result
console.log(result);
